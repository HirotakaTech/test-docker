FROM tomcat:9-jdk8

EXPOSE 8080

WORKDIR /usr/local/tomcat
COPY . ./webapps/
COPY ./postgresql-42.2.10.jar ./lib


CMD ["catalina.sh","run"]
